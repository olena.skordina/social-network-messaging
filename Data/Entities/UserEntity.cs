﻿using System.Collections.Generic;

namespace Data.Entities
{
    public class UserEntity
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public RoleEntity Role { get; set; }
        public ICollection<MessageEntity> MessageEntities { get; set; }
    }
}
