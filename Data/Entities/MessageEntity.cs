﻿using System;

namespace Data.Entities
{
    public class MessageEntity
    {
        public int Id { get; set; }
        public string MessageText { get; set; }
        public DateTime CreatedOn { get; set; }
        public int IsDeleted { get; set; }
        public int UserId { get; set; }
        public UserEntity UserEntity { get; set; }
    }
}
