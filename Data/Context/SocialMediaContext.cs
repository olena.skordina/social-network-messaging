﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data.Context
{
    public class SocialMediaContext:DbContext
    {
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<RoleEntity> Roles { get; set; }
        public DbSet<MessageEntity> Messages { get; set; }

        public SocialMediaContext(DbContextOptions<SocialMediaContext> options)
        : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        { 
            modelBuilder.Entity<UserEntity>(u =>
            {
                u.HasKey(x => x.Id);
                u.Property(x => x.Username).HasColumnType("varchar(256)").IsRequired();
                u.Property(x => x.Password).HasColumnType("varchar(256)").IsRequired();
                
            });

            modelBuilder.Entity<MessageEntity>(m =>
            {
                m.HasKey(x => x.Id);
                m.Property(x => x.MessageText).IsRequired();
                m.Property(x => x.CreatedOn).HasDefaultValueSql("GETDATE()");
                m.Property(x => x.IsDeleted).HasDefaultValue(0);
            });

            modelBuilder.Entity<MessageEntity>()
                .HasOne<UserEntity>(m => m.UserEntity)
                .WithMany(g => g.MessageEntities)
                .HasForeignKey(u=>u.UserId);
                
            modelBuilder.Entity<RoleEntity>()
                .HasData(new RoleEntity { Id = 1, RoleName = "admin" });
            modelBuilder.Entity<RoleEntity>()
                .HasData(new RoleEntity { Id = 2, RoleName = "user" });
            modelBuilder.Entity<UserEntity>()
                .HasData(new UserEntity { Id = 1, Username = "admin", Password = "admin", RoleId = 1 });
           
        }

    }
}
