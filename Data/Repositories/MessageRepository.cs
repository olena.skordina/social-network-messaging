﻿using Data.Context;
using Data.Entities;
using Data.Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Data.Repositories
{
    public class MessageRepository: IMessageRepository
    {
        private SocialMediaContext _socialMediaContext;

        public MessageRepository(SocialMediaContext socialMediaContext)
        {
            _socialMediaContext = socialMediaContext;
        }

        public async Task<MessageEntity> AddAsync(MessageEntity messageEntity)
        {
            var messageAdded = await _socialMediaContext.Messages.AddAsync(messageEntity);
            await _socialMediaContext.SaveChangesAsync();
            return messageAdded.Entity;
        }
        public async Task<List<MessageEntity>> GetAllMessagesAsync()
        {
            return await _socialMediaContext
                .Messages
                .Where(x => x.IsDeleted == 0)
                .OrderByDescending(x => x.CreatedOn)
                .ToListAsync();
        }

        public async Task<List<MessageEntity>> GetAllMessagesForUserAsync(int userId)
        {
            return await _socialMediaContext
                .Messages
                .Where(x =>x.UserId == userId && x.IsDeleted==0)
                .OrderByDescending(x=>x.CreatedOn)
                .ToListAsync();
        }

        public async Task<MessageEntity> GetMessageByIdAsync (int messageId)
        {
            return await _socialMediaContext
                .Messages
                .FirstOrDefaultAsync(x => x.Id == messageId && x.IsDeleted != 1);
        }

        public async Task UpdateMessageAsync(MessageEntity messageEntity)
        {
            var messageForUpdate = await GetMessageByIdAsync(messageEntity.Id);

            if (messageForUpdate != null)
                messageForUpdate.MessageText = messageEntity.MessageText;

            await _socialMediaContext.SaveChangesAsync();
        }

        public async Task DeleteMessageAsync(int messageId)
        {
            var messageForUpdate = await _socialMediaContext
                .Messages
                .FirstOrDefaultAsync(x => x.Id == messageId);

            messageForUpdate.IsDeleted = 1;
            await _socialMediaContext.SaveChangesAsync();
        }

    }
}
