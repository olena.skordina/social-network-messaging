﻿using Data.Context;
using Data.Entities;
using Data.Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private SocialMediaContext _socialMediaContext;

        public UserRepository(SocialMediaContext socialMediaContext)
        {
            _socialMediaContext = socialMediaContext;
        }

        public async Task<UserEntity> SaveUserAsync(UserEntity userEntity)
        {
            var userAdded = await _socialMediaContext.Users.AddAsync(userEntity);
            await _socialMediaContext.SaveChangesAsync();
            return userAdded.Entity;
        }

        public async Task<List<UserEntity>> GetUsersAsync()
        {
            return await _socialMediaContext.Users
                .ToListAsync();
        }

        public Task<RoleEntity> GetUserRoleByNameAsync (string roleName)
        {
            return _socialMediaContext
                .Roles
                .FirstOrDefaultAsync(x => x.RoleName == roleName);
        }

        public Task<UserEntity> GetUserByCredentialsAsync(string login, string password)
        {
            return _socialMediaContext
                .Users
                .Include(x => x.Role)
                .FirstOrDefaultAsync(x => x.Username == login && x.Password == password);
        }

        public Task<UserEntity> GetUserByLoginAsync(string login)
        {
            return _socialMediaContext
                .Users
                .Include(x => x.Role)
                .FirstOrDefaultAsync(x => x.Username == login);
        }
    }
}
