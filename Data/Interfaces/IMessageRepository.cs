﻿using Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IMessageRepository
    {
        Task<List<MessageEntity>> GetAllMessagesAsync();
        Task<List<MessageEntity>> GetAllMessagesForUserAsync(int userId);
        Task<MessageEntity> GetMessageByIdAsync(int messageId);
        Task<MessageEntity> AddAsync(MessageEntity messageEntity);
        Task UpdateMessageAsync(MessageEntity messageEntity);
        Task DeleteMessageAsync(int messageId);
    }
}
