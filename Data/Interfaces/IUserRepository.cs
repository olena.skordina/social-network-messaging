﻿using Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IUserRepository
    {
        Task<UserEntity> SaveUserAsync(UserEntity userEntity);
        Task<List<UserEntity>> GetUsersAsync();
        Task<UserEntity> GetUserByCredentialsAsync(string login, string password);
        Task<UserEntity> GetUserByLoginAsync(string login);
        Task<RoleEntity> GetUserRoleByNameAsync(string roleName);
    }
}
