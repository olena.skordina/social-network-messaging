﻿using Business.Requests;
using Business.Responses;
using Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IUserService
    {
        Task<EntityCreatedResponse> SaveUserAsync(UserRequest user);
        Task<List<UserResponse>> GetAllUsersAsync();
        Task<UserEntity> GetUserByCredentials(string login, string password);
    }
}
