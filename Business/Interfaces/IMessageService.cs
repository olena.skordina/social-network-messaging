﻿using Business.Requests;
using Business.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IMessageService
    {
        Task<EntityCreatedResponse> SaveMessageAsync(MessageRequest game, string userName);
        Task<List<MessageResponse>> GetAllMessagesForUserAsync(string userName);
        Task UpdateMessageAsync(MessageRequest message, string userName);
        Task DeleteMessageByIdAsync(int messageId, string userName);
    }
}
