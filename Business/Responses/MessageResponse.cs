﻿using System;

namespace Business.Responses
{
    public class MessageResponse
    {
        public int Id { get; set; }
        public string MessageText { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
