﻿namespace Business.Responses
{
    public class EntityCreatedResponse
    {
        public int Id { get; set; }
    }
}
