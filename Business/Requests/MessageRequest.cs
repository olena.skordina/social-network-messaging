﻿namespace Business.Requests
{
    public class MessageRequest
    {
        public int Id { get; set; }
        public string MessageText { get; set; }
    }
}
