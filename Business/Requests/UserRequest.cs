﻿using System.ComponentModel.DataAnnotations;

namespace Business.Requests
    {
    public class UserRequest
    {
        [Required]
        [MinLength(1)]
        public string Username { get; set; }
        [Required] 
        [MinLength(1)]
        public string Password { get; set; }
    }
}
