﻿using AutoMapper;
using Business.Interfaces;
using Business.Requests;
using Business.Responses;
using Data.Entities;
using Data.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Services
{
    public class UserService: IUserService
    {
        private IUserRepository _userRepository { get; set; }
        IMapper _mapper { get; set; }

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<EntityCreatedResponse> SaveUserAsync(UserRequest user)
        {
            var entityToCreate = _mapper.Map<UserEntity>(user);
            entityToCreate.RoleId = (await _userRepository.GetUserRoleByNameAsync("user")).Id;
            return _mapper.Map<EntityCreatedResponse>(await _userRepository.SaveUserAsync(entityToCreate));
        }

        public async Task<List<UserResponse>> GetAllUsersAsync()
        {
            return _mapper.Map<List<UserResponse>>(await _userRepository.GetUsersAsync());
        }

        public async Task<UserEntity> GetUserByCredentials(string login, string password)
        {
            return await _userRepository.GetUserByCredentialsAsync(login, password);
        }

    }
}
