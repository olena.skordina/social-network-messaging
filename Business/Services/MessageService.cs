﻿using AutoMapper;
using Business.Interfaces;
using Business.Requests;
using Business.Responses;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Services
{
    public class MessageService: IMessageService
    {
        private IMessageRepository _messageRepository { get; set; }
        private IUserRepository _userRepository { get; set; }
        IMapper _mapper { get; set; }

        public MessageService(IMessageRepository messageRepository, IUserRepository userRepository, IMapper mapper)
        {
            _messageRepository = messageRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }
        public async Task<EntityCreatedResponse> SaveMessageAsync(MessageRequest message, string userName)
        {
            var userEntity = await _userRepository.GetUserByLoginAsync(userName);

            if (userEntity!=null)
            {
                return _mapper.Map<EntityCreatedResponse>(await _messageRepository.AddAsync(new MessageEntity { MessageText = message.MessageText, UserId = userEntity.Id }));
            }
            else
            {
                throw new ArgumentException($"User {userName} is not exists");
            }
        }

        public async Task<List<MessageResponse>> GetAllMessagesForUserAsync(string userName)
        {
            var userEntity = await _userRepository.GetUserByLoginAsync(userName);

            if (userEntity.Role.RoleName == "admin")
            {
                return _mapper.Map<List<MessageResponse>>(await _messageRepository.GetAllMessagesAsync());
            }
            else
            {
                return _mapper.Map<List<MessageResponse>>(await _messageRepository.GetAllMessagesForUserAsync(userEntity.Id));
            }
        }

        public async Task UpdateMessageAsync(MessageRequest message, string userName)
        {
            var userEntity = await _userRepository.GetUserByLoginAsync(userName);
            var messageForUpdate = await _messageRepository.GetMessageByIdAsync(message.Id);

            if (messageForUpdate != null)
            {
                if (userEntity.Role.RoleName == "admin")
                {
                    await _messageRepository.UpdateMessageAsync(_mapper.Map<MessageEntity>(message));
                }
                else
                {
                    if (userEntity.Id == messageForUpdate.UserId)
                        await _messageRepository.UpdateMessageAsync(_mapper.Map<MessageEntity>(message));
                    else
                        throw new ArgumentException("User can update only own messages");
                }
            }
            else
                throw new ArgumentException("Message cannot be updated");
        }

        public async Task DeleteMessageByIdAsync(int messageId, string userName)
        {
            var userEntity = await _userRepository.GetUserByLoginAsync(userName);
            var messageToDelete = await _messageRepository.GetMessageByIdAsync(messageId);

            if (messageToDelete != null)
            {
                if (userEntity.Role.RoleName == "admin")
                {
                    await _messageRepository.DeleteMessageAsync(messageId);
                }
                else
                {
                    if (userEntity.Id == messageToDelete.UserId)
                        await _messageRepository.DeleteMessageAsync(messageId);
                    else
                        throw new ArgumentException("Message cannot be deleted");
                }
            }
            else
                throw new ArgumentException("Message cannot be deleted");

        }




    }
}
