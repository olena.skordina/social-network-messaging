﻿using AutoMapper;
using Business.Requests;
using Business.Responses;
using Data.Entities;

namespace Business
{
    public class ProfileAutoMapper : Profile
    {
        public ProfileAutoMapper()
        {
            CreateMap<MessageEntity, MessageRequest>()
                .ReverseMap();
            CreateMap<MessageEntity, MessageResponse>()
                .ReverseMap();
            CreateMap<MessageEntity, EntityCreatedResponse>()
               .ReverseMap();
            CreateMap<UserEntity, UserRequest>()
                .ReverseMap();
            CreateMap<UserEntity, UserResponse>()
                .ReverseMap();
            CreateMap<UserEntity, EntityCreatedResponse>()
              .ReverseMap();
        }
    }
}
