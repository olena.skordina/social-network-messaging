﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace SocialNetworkMessageWebAPI.Configuration
{
    public class AuthOptions
    {
        public const string ISSUER = "WebApplicationServer";
        public const string AUDIENCE = "WebApplicationClient";
        const string KEY = "SocialNetwork_verysecretkey! 123";
        public const int LIFETIME = 30;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
