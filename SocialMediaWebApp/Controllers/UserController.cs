﻿using Business.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using SocialNetworkMessageWebAPI.Configuration;
using Business.Requests;

namespace SocialNetworkMessageWebAPI.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> SaveUser([FromBody] UserRequest user)
        {
            var userResponse = await _userService.SaveUserAsync(user);
            return Created("api/users/" + userResponse.Id, userResponse);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<IActionResult> GetAllUsers()
        {
            var result = await _userService.GetAllUsersAsync();
            return Ok(result);
        }

        [AllowAnonymous]
        [Route("auth/token")]
        [HttpPost]
        public async Task<IActionResult> Token([FromBody] UserRequest user)
        {
            var identity = await GetIdentity(user.Username, user.Password);
            if (identity == null)
            {
                return BadRequest(new { errorText = "Invalid username or password" });
            }

            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(
                    AuthOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            var response = new
            {
                access_token = encodedJwt,
                username = identity.Name
            };
            return Json(response);
        }

        private async Task<ClaimsIdentity> GetIdentity(string username, string password)
        {
            var person =await _userService.GetUserByCredentials(username, password);
            if (person != null)
            {
                var claims = new List<Claim>
            {
                new Claim( ClaimsIdentity.DefaultNameClaimType, person.Username),
                new Claim( ClaimsIdentity.DefaultRoleClaimType, person.Role.RoleName)
            };
                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, " Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }
            return null;
        }
    }
}
