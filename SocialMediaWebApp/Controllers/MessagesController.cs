﻿using Business.Interfaces;
using Business.Requests;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace SocialNetworkMessageWebAPI.Controllers
{
    [Authorize(Roles = "admin, user")]
    [Route("api/messages")]
    public class MessagesController : ControllerBase
    {
        private readonly IMessageService _messageService;
        public MessagesController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [HttpPost]
        public async Task<IActionResult> SaveMessage ([FromBody] MessageRequest message)
        {
            var userName = HttpContext.User.Identity.Name;
            var savedMessageResponse = await _messageService.SaveMessageAsync(message, userName);
            return Created("api/messages/" + savedMessageResponse.Id, savedMessageResponse);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllMessages()
        {
            var userName = HttpContext.User.Identity.Name;
            var result = await _messageService.GetAllMessagesForUserAsync(userName);
            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateMessage([FromBody] MessageRequest message)
        {
            var userName = HttpContext.User.Identity.Name;
            await _messageService.UpdateMessageAsync(message, userName);
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteMessageById([FromBody] MessageRequest message)
        {
            var userName = HttpContext.User.Identity.Name;
            await _messageService.DeleteMessageByIdAsync(message.Id, userName);
            return Ok();
        }

    }
}
